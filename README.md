Les "Dis ... c'est quoi ?"
--------------------------

une série de petites BD à usage pédagogique
pour expliquer le numérique Libre simplement.
https://debian-facile.org/projets/discestquoi

--------------------------
BDs réalisées par [arpinux](https://arpinux.org)
pour [Debian-Facile](https://debian-facile.org)
grâce au générateur de [Geektionnerd](https://framalab.org/gknd-creator)

--------------------------
images par Simon "Gee" Giraudot
aka [grisebouille](https://grisebouille.net/)

--------------------------
contributeurs :
captnfab, bendia, èfpé, LibreFaso

--------------------------
pour participer, rejoignez la
[discussion dédiée](https://debian-facile.org/viewtopic.php?id=29407)
sur le forum.

--------------------------
licence Creative Commons BY-SA © arpinux@2021-2023
